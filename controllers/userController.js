//set up dependencies

const User = require("../models/user.js");
const Course = require("../models/course.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt"); //used to encrypt user passwords


//check if the email exists
/*
	1. check for th email in the database
	2. send the result as a response (with error handling)
*/
/*
	it is conventional for devs to use Boolean in sending return responses especially with the backend application
*/
module.exports.checkEmail = (requestBody)=>{
	return User.find({email: requestBody.email}).then((result, error)=>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			if(result.length>0){
				return true;
			}
			else{
				return false
			}
		}
	})
}

/*
USER REGISTRATION
1. create a new User with the information from the requestBody
2. make sure that the password is encrypted
3. save the new user
*/

module.exports.registerUser = (requestBody)=>{
	let newUser = new User({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		age: requestBody.age,
		gender: requestBody.gender,
		email: requestBody.email,
		// hashSync is a function of bcrypt that encrypts the password, 10 is the number of rounds/times it runs the algorithm to the requestBody.password
		// maximum is 72 implementations of algorithm
		password: bcrypt.hashSync(requestBody.password, 10),
		mobileNo: requestBody.mobileNo
	})
	return newUser.save().then((saved, error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			return newUser;
			return true;
		}
	})
}

//USER LOGIN
/*
	BUSINESS LOGIC
	1. find if the email is existing in the database
	2. check if the password matches the credentials
*/

module.exports.userLogin = (requestBody)=>{
	return User.findOne({email: requestBody.email}).then((result)=>{
		if(result === null){
			return false;
		}else{
			// compareSync function - used to compare a non-encrypted password to an encrypted password and returns to a boolean response depending on the result
			/*
			What should be done after comparison:
				true - a token should be created since the user is existing in the database and password is correct
				false - the passwords do not match, thus a token should not be created
			*/
			const isPassCorrect = bcrypt.compareSync(requestBody.password, result.password);
			if(isPassCorrect){
				return {access: auth.createAccessToken(result.toObject())}
				//auth - imported auth.js
					//createAccessToken - function inside the auth.js to create access token
					// .toObject() - transforms the User into an object that can be used by our createAccessToken
			}
			else{
				return false;
			}
		}
	})
}

module.exports.getProfile = (data)=>{
	return User.findById(data.userId).then(result=>{
		if(result === null){
			return false;
		}
		else{
			result.password = "";
			return result;
		}
	})
}


module.exports.enrollCourse = async(data)=>{
	let isUserUpdated = await User.findById(data.userId).then(user =>{
		user.enrollments.push({courseId: data.courseId})
		return user.save().then((user,err)=>{
			if(err){
				return false;
			}
			else{
				return true;
			}

		})
	})
	
	if (isUserUpdated){
		return Course.findById(data.courseId).then(course =>{
		course.enrollees.push({userId: data.userId})
		return course.save().then((course,err)=>{
			if(err){
				return false;
			}
			else{
				return true;
			}
		})
	})
	}
	else{
		return false
	}

	




	/*return Course.findById(requestBody.courseId).then((result, error)=>{
		if(error){
			console.log(error);
			return `Not a course.`
		}
		else{
			console.log(result);
			return User.findById(userId).then((result,error)=>{
				console.log(result);
				if(result !== null){
					enrollments.push(requestBody.courseId);
					return enrollments.save().then((result,error)=>{
						if(error){
							return false;
						}
						else{
							return true;
						}
					})
				}else{
					return false;
				}
			})
		}
	})*/
}