const User = require("../models/user.js");
const Course = require("../models/course.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt"); //used to encrypt user passwords


/*
	ACTIVITY s34
	1. find the user in the database
		find out if the user is admin

	2. if the user is not an admin, send the response "You are not an admin"

	3. if the user is an admin, create the new Course object
			-save the object
				-if there are errors, return false
				-if there are no errors, return "Course created sucessfully"
*/

module.exports.registerCourse = (userId, userisAdmin, reqBody)=>{
	return User.findById(userId).then((result,error)=>{
		if(result.isAdmin === true){
			console.log(result.isAdmin)
			let newCourse = new Course(
			{
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})
			return newCourse.save().then((saved,error)=>{
				if(error){
					return false;
				}else{
					return `Course created successfully`
					return true;
				}
			})
		}
		else{
			return `You are not an admin`;
		}
	})
}

//retrieve all courses
module.exports.getAllCourses = ()=>{
	//set up an empty object to get all the documents in our database
	return Course.find({}).then(result=>{
		return result;
	})
}

module.exports.getCourse = (courseId)=>{
	return Course.findById(courseId).then((result,error)=>{
		if(error){
			return false;
		}
		else{
			return result;
		}
	})
}

//retrieve all active courses
module.exports.getActiveCourses = ()=>{
	return Course.find({isActive : true}).then((result,error )=> {
		if(error){
			return false;
		}
			else{
				return result
			}
	})
}

module.exports.updateCourse = (reqParams, reqBody)=>{
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
// findByIdAndUpdate - looks for the id of the document (firstparameter) and updates it (content of the second parameter)
	// the server will be looking for the id of the reqparams.courseid in the database and updates the document through the context of the object updated Course
	return Course.findByIdAndUpdate(reqParams.id, updatedCourse).then((result,error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

//archive a course

module.exports.archiveCourse = (reqParams,reqBody)=>{
	console.log(reqParams)
	console.log(reqBody)
	let updateCourse = {
		isActive: reqBody.isActive
	}
	// findByIdAndUpdate - looks for the id of the document (firstparameter) and updates it (content of the second parameter)
	// the server will be looking for the id of the reqparams.courseid in the database and updates the document through the context of the object updated Course
	return Course.findByIdAndUpdate(reqParams.id, updateCourse).then((result, error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}