const mongoose = require("mongoose");

//required inputs by the users
const courseSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, "Course Name is required."]
	},
	description:{
		type: String,
		required: [true, "Course description is required."]
	},
	/*
		create a similar data structure for price (number), isActive (boolean), createdOn(type: data, default value)
	*/
	price:{
		type: Number,
		required: [true, "Course price is required."]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},

	/*
		enrollees:
			{userId :1423421,
			enrolledOn: Apr 27, 2022}
	*/
	enrollees:[
	{
		userId: {
			type: String,
			required: [true, "User ID is required."]
		},
		enrolledOn:{
			type: Date,
			default: new Date()
		}
	}
	]
})

module.exports = mongoose.model("Course", courseSchema)