const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController.js")

// create a route thta will let an admin perform addCourse function in the courseController
// verify that the user is loggedin
// try to decode the token for that user
// use the ID, is admin, and request Body to perform the function in courseController
// 	the id and isAdmin are parts of an object

router.post("/", auth.verify, (req, res)=>{
	//auth.verify is to verify that the user is loggedin

	const userData = auth.decode(req.headers.authorization); // userData would now contin an object that has the token payload (id, email, isAdmin information of the user)
	console.log(userData);
	console.log(userData.id, userData.isAdmin, req.body)
	courseController.registerCourse(userData.id, userData.isAdmin, req.body).then(result=>res.send(result));
})

router.get("/", (req,res)=>{
	courseController.getAllCourses().then(resultFromController=>res.send(resultFromController));
})


//retrieve all active courses
router.get("/activeCourses", (req,res)=>{
	courseController.getActiveCourses().then(result=>res.send(result));
})

router.put("/:id/archiveCourse", auth.verify, (req, res)=>{
	courseController.archiveCourse(req.params, req.body).then(result=>res.send(result));
})

router.get("/:id", (req,res)=>{
	courseController.getCourse(req.params.id).then(result=>res.send(result));
})



router.put("/:id", auth.verify, (req, res)=>{
	courseController.updateCourse(req.params, req.body).then(result=>res.send(result));
})






module.exports = router;
