const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const userController = require("../controllers/userController.js");

/*
create a function with the "/checkEmail" endpoint that will be able to use the checkEmail function in the userController
reminder that we have a parameter needed in the checkEmail function and that is data from request Body
*/

router.post("/checkEmail", (req,res)=>{
	userController.checkEmail(req.body).then(result=>res.send(result));
})

//user registration
router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(result=>res.send(result));
})

router.post("/login", (req,res)=>{
	userController.userLogin(req.body).then(result=>res.send(result));
})

//auth.verify - ensures that a user is logged in before proceeding to the next part of the codel; this is the verify function inside the auth.js
router.get("/details", auth.verify, (req, res)=>{
	//decode - decrypts the token inside the authorization (which is the headers of the request)
	// req.headers.authorization contains the token that was created for the user
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getProfile({userId: userData.id}).then(result=>res.send(result));
});

// creating a new array so post is used
router.post("/enroll", auth.verify, (req,res)=>{
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId
	}
	console.log(data);
	userController.enrollCourse(data).then(result=>res.send(result));
})


module.exports = router;