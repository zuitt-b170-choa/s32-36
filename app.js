// setting up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// access to routes
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

// server
const app = express();
const port = 4000;

// allows all origins/domains to access the backend application 
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);

mongoose.connect("mongodb+srv://gabchoa:gabchoa123@wdc028-course-booking.c9sio.mongodb.net/b170-course-booking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database."));

app.listen(process.env.PORT || port, () => console.log(`API now online at port ${process.env.PORT || port}`));
// process.env.port works if you are deploying the API in a host like heroku. this code allows the app to use the environment of that host (heroku) in running the server